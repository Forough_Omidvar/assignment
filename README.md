**The unit test coverage report**

After an overview, it seems that the unit test is not written for the following cases:

•	Creating a new user (Sign up).

•	Retrieving information from all users .

•   Also, Only happy flow scenarios are written. while unit tests should also cover Corner Case Testing, Boundary Case Testing, Edge Case Testing, and Fuzz testing.

---

**How to run a test suit:**

•	Right -Click on **TestRunner** class on this path: **WAES\src\test\java\runnersTestRunner.java** and Click **Run As  >> JUnit Test**. Consequently, the result will appear in the left-hand side project explorer window in the JUnit tab.

---

**How to access the generated report:**

•	You can see the test report on this path: **WAES\target\cucumber-reports\index.html**

---

**Suggestion For improvement:**

•	Some error codes are not recorded in the Swagger document, such as 400 or 415.

•	In the header of update and delete  key-value Content-Type=application/json is needed
