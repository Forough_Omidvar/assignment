Feature: Test the functionality of the initiative APIs
Description: The purpose of these tests are to cover End to End happy flows for WAES heroes

WAES heroes swagger URL : http://localhost:8081/swagger-ui.html
Background: We have three user accounts with IDs 1, 2 and 3 that should not be removed from the database
		@regression
		Scenario: Create a new account and delete it
				Given The user is new
				When The client app attempts to send a Create new account Request
				Then The response Code should be 201
				And A unique identifier is generated for the user
				
				Given Retrieving the user information
				When The client app attempts to send a Delete account request and get the right msg
			  Then The response Code should be 200
			  
		@regression		
		Scenario: Registered user is able to Login and Update account
				Given The user has already registered
 				When The client app attempts to send a Login request 
 				Then The response Code should be 200 and User information is returned in JSON format
				
				When The client app attempts to send an Upadate request
				Then The response Code should be 200 and The data must have Changed	