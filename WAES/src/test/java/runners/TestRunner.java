/***************************
 * Author: Forough Omidvar *
 * fo.omidvar@gmail.com    *
 * Created: october-2020   *
 **************************/
package runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(

        features = "src/test/resources/functionalTests",
        glue = {"stepDefinitions"},
        tags = {"@regression"},
        monochrome = true,
        strict = true,
   		plugin = { "pretty" , "html:target/cucumber-reports" , 
   				"json:target/cucumber-reports/Cucumber.json",
   				"junit:target/cucumber-reports/Cucumber.xml"}
)
public class TestRunner {

}