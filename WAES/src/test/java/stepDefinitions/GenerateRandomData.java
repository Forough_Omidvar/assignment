/***************************
 * Author: Forough Omidvar *
 * fo.omidvar@gmail.com    *
 * Created: october-2020   *
 **************************/
package stepDefinitions;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import org.apache.commons.lang3.RandomStringUtils;

public class GenerateRandomData {
	public static String getUserName() {
		String generatedString = RandomStringUtils.randomAlphabetic(2);
		return("John" + generatedString);
	}
	public static String randomDataOfBirth(int yearStartInclusive, int yearEndExclusive) {
	    LocalDate start = LocalDate.ofYearDay(yearStartInclusive, 1);
	    LocalDate end = LocalDate.ofYearDay(yearEndExclusive, 1);

	    long longDays = ChronoUnit.DAYS.between(start, end);
	    int days = (int) longDays;
	    if (days != longDays) {
	        throw new IllegalStateException("int overflow; too many years");
	    }
	    int day = randBetween(0, days);
	    LocalDate dateOfBirth = start.plusDays(day);

	    return dateOfBirth.toString();
	}
	public static int randBetween(int start, int end) {
         return start + (int)Math.round(Math.random() * (end - start));
    }
	
	public static String getEmail() {
		String generatedString = RandomStringUtils.randomAlphabetic(3);
		return(generatedString + "@gmail.com");
	}
	
	public static String getName() {
		String generatedString = RandomStringUtils.randomAlphabetic(1);
		return("John" + generatedString);
	}
	
	public static String getPower() {
		String generatedString = RandomStringUtils.randomAlphabetic(4);
		return("Power_" + generatedString);
	}
}