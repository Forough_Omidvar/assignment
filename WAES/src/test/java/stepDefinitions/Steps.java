/***************************
 * Author: Forough Omidvar *
 * fo.omidvar@gmail.com    *
 * Created: october-2020   *
 **************************/
package stepDefinitions;

import java.util.HashMap;
import org.junit.Assert;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Steps {
	private static final String BASE_URL = "http://localhost:8081";
	private static final String[] USERNAMES = {"tester","dev"};
	private static final String[] PASSWORDS = {"maniac" , "wizard"};
	private static final String[] dev_INFO = {"2" , "Zuper Dooper Dev" ,"dev" , "wizard" , "zd.dev@wearewaes.com" , "Debug a repellent factory storage.","1999-10-10" , "false"};
	private static final String[] Tester_INFO = {"3" , "Al Skept-Cal Tester" , "tester","as.tester@wearewaes.com" , "Voltage AND Current." , "2014-07-15" , "false"};
	
	private static HashMap<String,String> map = new HashMap<>();
	private static Response response;
	private static String Username;
	private static String bodyAsString; 
	private static String NewSuperpower;
	
	@Given("The user is new")
	public  void TheUserIsNew() {
		System.out.println("/*************************************CREAITING ACCOUNT***********************************************/");
        // Create a JSON request which contains all the fields
		Username = GenerateRandomData.getUserName();
		map.put("username", Username);
		map.put("isAdmin", "false");
		map.put("dateOfBirth", GenerateRandomData.randomDataOfBirth(1900, 2015));
		map.put("email",GenerateRandomData.getEmail());
		map.put("name", GenerateRandomData.getName());
		map.put("password", "123456");
		map.put("superpower", GenerateRandomData.getPower());
	}
	
	@When("The client app attempts to send a Create new account Request")
	public void CreateAccountRequest() {
    	// Create a Request pointing to the Service Endpoint
		RestAssured.baseURI = BASE_URL;
	    RequestSpecification request = RestAssured.given();
        // Add a header stating the Request body is a JSON
	    request.header("Content-Type", "application/json");
	    // Post the request
	    response = request.body(map).post("/waesheroes/api/v1/users");
	}
	
	@Then("The response Code should be 201")
	public void VerifyResponseCode201() {
		// Validate the Response
		Assert.assertEquals(201, response.getStatusCode());
		System.out.println("The new Account genereted and Response body is:\n " + response.body().asString());
        System.out.println("The status code recieved from Sign up API is: " + response.getStatusCode());
	}
	
	@And("A unique identifier is generated for the user")
	public void Verifyid() {
		// Check that the account has been created and has received an id
		try {
			String bodyAsString = response.body().asString();
			Assert.assertEquals(bodyAsString.contains("id"), true);
		}catch (Exception e) {
			System.out.println("No new account created!");
		}
		System.out.println("/***********************************END OF CREAITING ACCOUNT******************************************/");

	}
	
	@Given ("Retrieving the user information")
	public void GetInfo() {
		System.out.println("/*****************************************DELETE ACCOUNT**********************************************/");
		// Get new user information
	    RestAssured.baseURI = BASE_URL;
		RequestSpecification request = RestAssured.given();
		response = request.get("/waesheroes/api/v1/users/details?username=" + Username);
		bodyAsString = response.body().asString();
		System.out.println("The user information we want to delete is:\n " + response.body().asString());
	}
	
		@When("The client app attempts to send a Delete account request and get the right msg")
		public void DeleteAccount() {
		//Send a delete request
	    RestAssured.baseURI = BASE_URL;
	    RequestSpecification request = RestAssured.given().auth().preemptive().basic(Username, "123456").body(bodyAsString);
	    request.header("Content-Type", "application/json");
	    Response response = request.delete("/waesheroes/api/v1/users");
	    System.out.println("Response after send Delete Request is:" + response.body().asString());
	    Assert.assertEquals(response.body().asString().contains("User '" + Username + "' removed from database."), true);
	}

	@Then("The response Code should be 200")
	public void CheckResponseCode200() {
		Assert.assertEquals(200, response.getStatusCode());
        System.out.println("The status code recieved from Delete API is: " + response.getStatusCode());
        System.out.println("/***********************************END OF DELETE ACCOUNT*********************************************/");
	}
	
	@Given("The user has already registered")
	public void GetUserInfo() {
		System.out.println("/************************************************LOGIN************************************************/");
		// Create a Request pointing to the Service Endpoint(View details about a user)
		RestAssured.baseURI = BASE_URL;
		RequestSpecification request = RestAssured.given();
		response = request.get("/waesheroes/api/v1/users/details?username=" + USERNAMES[0]);
		// Check that the user has already registered
				try {
					String bodyAsString = response.body().asString();
					Assert.assertEquals(bodyAsString.contains(USERNAMES[0]), true);
				    System.out.println("The information of the user who wants to login is:\n " + response.body().asString());

				}catch (Exception e) {
					System.out.println("There is no account for this user: " + USERNAMES[0] );
				}
	}
	@When("The client app attempts to send a Login request")
	public void LoginRequest() {
		// Specify the base URL to the RESTful web service
	    RestAssured.baseURI = BASE_URL;
	    RequestSpecification request  = RestAssured.given().auth().preemptive().basic(USERNAMES[0], PASSWORDS[0]);
	    response = request.get("/waesheroes/api/v1/users/access");
	    
	}
	
	@Then("The response Code should be 200 and User information is returned in JSON format")
	public void VerifyResponseCode200() {
		int statusCode = response.getStatusCode();
		// Validate the Response
		Assert.assertEquals(200, statusCode);
	    System.out.println("The status code recieved from Login API is: " + statusCode);

		JsonPath jsonPathEvaluator = response.jsonPath();
		
		int id = jsonPathEvaluator.get("id");
		Assert.assertEquals(Integer.toString(id) , Tester_INFO[0]);
		
		String name = jsonPathEvaluator.get("name");
		Assert.assertEquals(name , Tester_INFO[1]);
		
		String username = jsonPathEvaluator.get("username");
		Assert.assertEquals(username , Tester_INFO[2]);
		
		String email = jsonPathEvaluator.get("email");
		Assert.assertEquals(email , Tester_INFO[3]);
		
		String superpower = jsonPathEvaluator.get("superpower");
		Assert.assertEquals(superpower , Tester_INFO[4]);
		
		String dateOfBirth = jsonPathEvaluator.get("dateOfBirth");
		Assert.assertEquals(dateOfBirth , Tester_INFO[5]);
		
		boolean isAdmin = jsonPathEvaluator.get("isAdmin");
		Assert.assertEquals(Boolean.toString(isAdmin) , Tester_INFO[6]);
		
		System.out.println("/***********************************************END OF LOGIN******************************************/");
	}
	
	@When("The client app attempts to send an Upadate request")
	public void UpdateRequest() {  
		System.out.println("/***************************************************UPDATE********************************************/");
		// Get the value of user's Superpower field
		RestAssured.baseURI = BASE_URL;
		RequestSpecification request = RestAssured.given();
		response = request.get("/waesheroes/api/v1/users/details?username=" + USERNAMES[1]);
		JsonPath jsonPathEvaluator = response.jsonPath();
		String OldSuperpower = jsonPathEvaluator.get("superpower");
		System.out.println("The value of superpower field before change is: " + OldSuperpower );
		
		// Create a JSON request which contains all the fields
		map.put("id", dev_INFO[0]);
        map.put( "name", dev_INFO[1]);  
        map.put("username", dev_INFO[2]);
        map.put("password", dev_INFO[3]);
        map.put("email", dev_INFO[4]);

        NewSuperpower = GenerateRandomData.getPower();
        System.out.println ("Randome Value is:" + NewSuperpower); 
        
        map.put("superpower", NewSuperpower);
        map.put("dateOfBirth", dev_INFO[6]);
        map.put("isAdmin", dev_INFO[7]);
    
        // Specify the base URL to the RESTful web service
		RequestSpecification newrequest = RestAssured.given();
	    newrequest.header("Content-Type", "application/json");
	    response = newrequest.auth().preemptive().basic(USERNAMES[1], PASSWORDS[1]).body(map).put("/waesheroes/api/v1/users");
        System.out.println("The value of superpower field after change is: "  + map.get("superpower"));
	}
	
	@Then("The response Code should be 200 and The data must have Changed")
	public void VerifyResponseAndCheckData(){
		int statusCode = response.getStatusCode();
		// Validate the Response
		Assert.assertEquals(200, statusCode);
	    System.out.println("The status code recieved from Update API is: " + statusCode);

		RestAssured.baseURI = BASE_URL;
		RequestSpecification request = RestAssured.given();
		response = request.get("/waesheroes/api/v1/users/details?username=" + USERNAMES[1]);
		JsonPath jsonPathEvaluator = response.jsonPath();
		String NewsuperpowerValue = jsonPathEvaluator.get("superpower");
		System.out.println("The value of superpower field after change in DataBase is: " + NewsuperpowerValue);
		Assert.assertEquals(NewSuperpower , NewsuperpowerValue);
		
		System.out.println("/***********************************************END OF UPDATE****************************************/");
		}
}