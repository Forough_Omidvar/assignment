$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/functionalTests/API_Test.feature");
formatter.feature({
  "name": "End to Ebd Tests for ToolsQA\u0027s Book Store API",
  "description": "Description: the purpose is ...\nBookStore swagger URL: http://...",
  "keyword": "Feature"
});
formatter.background({
  "name": "user generates token for Authorisation",
  "description": "\t\tGiven: I am an aouthorised user",
  "keyword": "Background"
});
formatter.scenario({
  "name": "Autorised user is able to add nad remove book.",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A list of books are available",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "I add a book to my reading list",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The book is added",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "I remove a book from my reading list",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The book is removed",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});